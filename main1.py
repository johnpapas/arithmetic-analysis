# -*- coding: utf-8 -*-
"""
Created on Tue May 18 02:36:40 2021

@author: johni
"""

import leftParallelogramm as LP 
import rightParallelogramm as RP
import trapezoid as trap
import simpson as simp
import math
import numpy as np
from matplotlib import pyplot

func=lambda x: ((math.e)**(3*x))*np.sin(2*x)

a=0
b=math.pi/4
result=(3*((math.e)**(3*math.pi/4)))/13+(2/13)
h=np.logspace(-1,-7,8)
lp=np.zeros((8,1))
rp=np.zeros((8,1))
tr=np.zeros((8,1))
si=np.zeros((8,1))

for i in range(0,8,1):
    lp[i]=LP.complexLeftParallelogram(h[i],a,b,func)
    rp[i]=RP.complexRightParallelogram(h[i],a,b,func)
    tr[i]=trap.complexTrapezoid(h[i],a,b,func)
    si[i]=simp.complexSimpson(h[i],a,b,func)
    # si[i]=simp.simpson(a,b,h[i],func)


errorLp=abs(lp-result)
errorRp=abs(rp-result)
errorTrap=abs(tr-result)
errorSimp=abs(si-result)

pyplot.loglog(h,errorLp)
pyplot.loglog(h,errorRp)
pyplot.loglog(h,errorTrap)
pyplot.loglog(h,errorSimp)
pyplot.legend(["Left Parallelogram", "Right Parallelogram", "Trapezoid", "Simpson"])
