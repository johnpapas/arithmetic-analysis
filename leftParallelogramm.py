# -*- coding: utf-8 -*-
"""
Created on Tue May 18 01:26:15 2021

@author: johni
"""

import numpy as np
import math

def leftParallelogram(a,b,func):
    return (b-a)*func(a)

def complexLeftParallelogram(h,a,b,func):
    N=math.floor((b-a)/h)
    # vec=np.arange(a,b,h)
    vec=np.linspace(a,b,N+1)
    vecFunc=func(vec)
    return h*sum(vecFunc[0:len(vecFunc)-2])


  
    