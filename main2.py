# -*- coding: utf-8 -*-
"""
Created on Wed May 19 16:31:23 2021

@author: johni
"""

import euler
import numpy as np
from matplotlib import pyplot as pp

func=lambda t,x: np.array([(1.1*x[0]-0.4*x[0]*x[1]), 0.4*x[0]*x[1]-0.1*x[1]])

x0=[20,1]
tmax=200
dt=0.0001
t0=0

k=euler.euler(func,x0,t0,tmax,dt)
t=k[0]
x=k[1]

pp.plot(t,x[:, 0],'b-',t,x[:, 1],'r--')
pp.grid()
pp.title('Time Evolution of Foxes Population and Rabbits Population')
pp.legend(['Rabbits','Foxes'])
pp.xlabel('time')
pp.ylabel('No. of Rabbits and No. of Foxes' )
pp.show()

pp.plot(x[:,0], x[:,1], 'k--')
pp.grid()
pp.title('Phase Plot')
pp.xlabel('Rabbits')
pp.ylabel('Foxes')
pp.show