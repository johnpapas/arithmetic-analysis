# -*- coding: utf-8 -*-
"""
Created on Tue May 18 02:05:23 2021

@author: johni
"""
import numpy as np
import math

def rightParallelogram(a,b,func):
    return (b-a)*func(b)

def complexRightParallelogram(h,a,b,func):
    N=math.floor((b-a)/h)
    # vec=np.arange(a,b,h)
    vec=np.linspace(a,b,N+1)
    vecFunc=func(np.array(vec))
    return h*sum(vecFunc[1:len(vecFunc)-1])

