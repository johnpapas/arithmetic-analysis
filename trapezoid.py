# -*- coding: utf-8 -*-
"""
Created on Tue May 18 02:08:27 2021

@author: johni
"""

import numpy as np
import math

def trapezoid(a,b,func):
    return (b-a)*(func(a)+func(b))/2

def complexTrapezoid(h,a,b,func):
    N=math.floor((b-a)/h)
    result=0.5*func(a)+0.5*func(b)
    for i in range(1,N):
        result += func(a+i*h)
    result *=h
    return result

