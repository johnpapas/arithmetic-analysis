# -*- coding: utf-8 -*-
"""
Created on Tue May 18 02:24:01 2021

@author: johni
"""

import numpy as np
import math

def simpson(a,b,func):
    return (b-a)*(func(a)+func(b)+4*func((a+b)/2))/6

def complexSimpson(h,a,b,func):
    N=math.floor((b-a)/h)
    vec=np.arange(a,b,h/2)
    # vec=np.linspace(a,b,2*N+1)
    vecFunc=func(np.array(vec))
    t=len(vecFunc)
    even=np.arange(2,t-3,2)
    odd=np.arange(1,t-2,2)
    return h*(vecFunc[0]+vecFunc[t-1]+2*sum(vecFunc[even])+4*sum(vecFunc[odd]))/6
